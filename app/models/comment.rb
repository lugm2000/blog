class Comment < ActiveRecord::Base
	belongs_to :post
	validates_presence_of :body, :on => :create, :message => "can't be blank"
	validates_presence_of :post_id, :on => :create, :message => "can't be blank"
end